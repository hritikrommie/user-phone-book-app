﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User_Phone_Book_App.Models;

namespace User_Phone_Book_App.Repository
{
    public interface IUserRepository
    {
        void AddUser(User user);
        User GetAUserByName(string name);
        User[] GetAllUsers();
        User[] GetAllActiveUsers();
        void BlockUser(String name);

    }
}
