﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User_Phone_Book_App.Exceptions;

namespace User_Phone_Book_App.Models
{
    public class User
    {
        // Sorted list for storing
        // phone number and number status
        // active = true : inactive = false 
        SortedList<long, bool> _phoneNumbers;

        // Constructor
        public User()
        {
            _phoneNumbers = new SortedList<long, bool>();
        }

        #region Properties
        public string Name { get; set; }
        public int CountryCode { get; set; }
        public bool IsUserActive { get; set; } = true;
        #endregion

        // Functionalities
        
        // Add a new phone number to user directory
        public void AddPhoneNumber(long number)
        {
            // If number already exist and it is inactive
            // then we will make it active
            try
            {
                if (_phoneNumbers.ContainsKey(number))
                {
                    if (!_phoneNumbers[number])
                    {
                        _phoneNumbers[number] = true;
                    }
                    else
                    {
                        throw new PhoneNumberAlreadyExistException("Phone number is already exist and active.");
                    }
                }
                else
                {
                    _phoneNumbers.Add(number, true);
                }
            }
            catch(PhoneNumberAlreadyExistException pnaex)
            {
                Console.WriteLine(pnaex.Message);
            }
        }

        // Get all active as well as inactive
        // phone numbers of that particular user
        public long[] GetAllPhoneNumbers()
        {
            long[] allPhoneNumbers = new long[_phoneNumbers.Count];
            _phoneNumbers.Keys.CopyTo(allPhoneNumbers,0);
            return allPhoneNumbers;
        }

        // Get all active phone numbers of that particular user
        public List<long> GetAllActivePhoneNumbers()
        {
            long[] allPhoneNumbers = GetAllPhoneNumbers();
            List<long> allNumbers = allPhoneNumbers.ToList();
            return allNumbers.FindAll(number => _phoneNumbers[number]);
        }

        // Set an active phone number to inactive
        public void SetAPhoneNumberInactive(long number)
        {
            if (_phoneNumbers.ContainsKey(number))
            {
                _phoneNumbers[number] = false;
            }
            else
            {
                _phoneNumbers.Add(number, false);
            }
        }
        
        // Remove all inactive phone numbers from user's directory
        public void ClearAllInactivePhoneNumber()
        {
            List<long> activeNumbers = GetAllActivePhoneNumbers();
            _phoneNumbers.Clear();
            foreach (long number in activeNumbers)
            {
                _phoneNumbers.Add(number, true);
            }
        }

        // Get any random phone number of that user
        public long GetAnyPhoneNumber() 
        {
            List<long> allActiveNumber = GetAllActivePhoneNumbers();
            var random = new Random();
            int index = random.Next(allActiveNumber.Count);
            return allActiveNumber[index];

        }

        // Get any random phone number with country code
        public string GetAnyActivePhoneNumberWithCountryCode()
        {
            List<long> allActiveNumber = GetAllActivePhoneNumbers();
            if (allActiveNumber.Count == 0)
            {
                return $"{CountryCode}-00000000000";
            }
            var random = new Random();
            int index = random.Next(allActiveNumber.Count);
            return $"{CountryCode}-{allActiveNumber[index]}";
        }



        // To string function
        public override string ToString()
        {
            return 
                $"Name : {Name}\n" +
                $"Country Code : +{CountryCode}\n" +
                $"Is User Active : {IsUserActive}\n" +
                $"Phone Number : {GetAnyActivePhoneNumberWithCountryCode()}";
        }
    }
}
