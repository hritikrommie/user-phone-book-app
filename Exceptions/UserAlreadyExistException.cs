﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User_Phone_Book_App.Exceptions
{
    internal class UserAlreadyExistException:ApplicationException
    {
        public UserAlreadyExistException()
        {

        }
        public UserAlreadyExistException(string message):base(message)  
        {

        }
    }
}
