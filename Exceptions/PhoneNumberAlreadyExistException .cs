﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User_Phone_Book_App.Exceptions
{
    internal class PhoneNumberAlreadyExistException:ApplicationException
    {
        public PhoneNumberAlreadyExistException()
        {

        }
        public PhoneNumberAlreadyExistException(string message):base(message)
        {

        }
    }
}
