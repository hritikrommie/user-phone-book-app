﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using User_Phone_Book_App.Models;
using User_Phone_Book_App.Repository;

namespace User_Phone_Book_App
{
    internal class Program
    {
        static void Main(string[] args)
        {
            
            User user1 = new User() { Name = "user1",CountryCode=91};
            user1.AddPhoneNumber(232321123);
            user1.AddPhoneNumber(232321124);
            user1.AddPhoneNumber(232321125);
            long[] a = user1.GetAllPhoneNumbers();
            foreach(long i in a)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine();


            user1.SetAPhoneNumberInactive(232321124);
            List<long> b = user1.GetAllActivePhoneNumbers();
            foreach(long i in b)
            {
                Console.WriteLine(i);
            }

            user1.ClearAllInactivePhoneNumber();
            long[] c = user1.GetAllPhoneNumbers();
            foreach(long i in c)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine($"phone : {user1.GetAnyPhoneNumber()}");

            Console.WriteLine(user1);

            UserRepository manage = new UserRepository();
            manage.AddUser(user1);

            User user2 = new User() { Name = "user2", CountryCode = 93 };
            User user3 = new User() { Name = "user3", CountryCode = 92 };
            
            manage.AddUser(user2);
            manage.AddUser(user3);

            User[] activeUser = manage.GetAllActiveUsers();
            Console.WriteLine("Active Uses : ");
            foreach(User u in activeUser)
                Console.WriteLine(u);

            manage.BlockUser("user1");
            activeUser = manage.GetAllActiveUsers();

            Console.WriteLine("Active Uses 2: ");
            foreach (User u in activeUser)
                Console.WriteLine(u);

        }
    }
}
