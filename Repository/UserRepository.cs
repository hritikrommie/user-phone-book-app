﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User_Phone_Book_App.Exceptions;
using User_Phone_Book_App.Models;

namespace User_Phone_Book_App.Repository
{
    public class UserRepository : IUserRepository
    {
        // A list for storing all users
        List<User> _users;

        // Constructor
        public UserRepository()
        {
            _users = new List<User>();   
        }

        // Add a new user to directory
        public void AddUser(User user)
        {
            User searchedUser = GetAUserByName(user.Name);
            try
            {
                if(searchedUser == null)
                {
                    _users.Add(user);
                    CreateUserFileAndStoreData(user);   
                }
                else
                {
                    throw new UserAlreadyExistException($"User with name {user.Name} already exist.");
                }
            }
            catch(UserAlreadyExistException uaex)
            {
                Console.WriteLine(uaex.Message);
            }

        }

        // Get all users which are active
        public User[] GetAllActiveUsers()
        {
            return _users.FindAll(u=>u.IsUserActive==true).ToArray();   
        }

        // Get an array of all users
        public User[] GetAllUsers()
        {
            return _users.ToArray();
        }

        // Search a user by name
        public User GetAUserByName(string name)
        {
            return _users.Find(u => u.Name == name);
        }

        // Change the status of given user to false
        public void BlockUser(String name)
        {
            foreach(User user in _users)
            {
                if(user.Name == name)
                {
                    user.IsUserActive = false;
                    break;
                }
            }
        }

        // Create seperate file for each user in user's data folder
        void CreateUserFileAndStoreData(User user)
        {
            string cd = Directory.GetCurrentDirectory();
            string path = cd.Substring(0, cd.IndexOf("bin") - 1) + @"\User's Data";
            string filePath;
            
            if (Directory.Exists(path))
            {
                filePath = path + @$"\{user.Name}.txt";
                var myFile  = File.Create(filePath);
                myFile.Close();
            }
            else
            {
                Directory.CreateDirectory(path);
                filePath = path + @$"\{user.Name}.txt";
                var myFile = File.Create(filePath);
                myFile.Close();
            }
            
            string[] user_data = user.ToString().Split("\n");
            File.WriteAllLines(filePath, user_data);
        }
        
    }
}
